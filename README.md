# less1.8

Данное решение подходит если нужно, чтобы у обоих nginx был одинаковый конфиг и имеенно конфижек монтировался с помощью volume external
```
# создаем volume
docker volume create nginx1

# смотрим в какой дире на тачке он создался
docker volume inspect nginx1

# копируем в диру с volume конфиг nginx
cp nginx/nginx1.conf /path/to/your/volume
```

# P.S

Все таки можно сделать именнованный volume и указать путь до файла/диры и т.д.
Полезная ссылочка: https://blog.code4hire.com/2018/06/define-named-volume-with-host-mount-in-the-docker-compose-file/